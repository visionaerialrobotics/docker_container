# Docker container for Aerostack

## Introduction

Docker is a platform to develop, deploy, and run applications with containers. The use of Linux containers to deploy applications is called containerization. Docker performs operating-system-level virtualization. Docker can package an application and its dependencies in a virtual "container" that can run on any Linux server. This helps enable flexibility and portability on where the application can run.

Containerization is increasingly popular because containers are:

* Flexible: Even the most complex applications can be containerized.
* Lightweight: Containers leverage and share the host kernel.
* Interchangeable: You can deploy updates and upgrades on-the-fly.
* Portable: You can build locally, deploy to the cloud, and run anywhere.
* Scalable: You can increase and automatically distribute container replicas.
* Stackable: You can stack services vertically and on-the-fly.

A container is launched by running an "image". A Docker image is a read-only template used to build containers. An image is an executable package that includes everything needed to run an application--the code, a runtime, libraries, environment variables, and configuration files. Images are used to store and ship applications. Containers are created from "images" that specify their precise contents.

A Docker "container" is a standardized, encapsulated environment that runs applications. A container is managed using the Docker API or CLI (command line interpreter). A container is a runtime instance of an image--what the image becomes in memory when executed (that is, an image with a state, or a user process).  Containers are run by a single operating system kernel and are more lightweight than virtual machines. 

To become familiar with Docker environment you can consult the following documentation:

* [Start with Docker](https://docs.docker.com/get-started/)

## Preparation

Install Docker in your computer:

* [Docker installation](https://docs.docker.com/install/#supported-platforms)


Clone in your working directory the repository with the scripts that create and use the Aerostack Docker container:
    
```sh
$ git clone https://bitbucket.org/visionaerialrobotics/docker_container.git
```

It includes the following scripts:

* `build.sh`: Creates the Docker image for Aerostack.

* `start.sh`: Starts the Aerostack Docker image.

* `launch.sh`: Launches the Aerostack Docker image. 

* `stop.sh`: Stops the running container 

You need to use `sudo` to execute these Docker scripts in a terminal window.
If you want to use Docker as a non-root user (once Docker is installed) you should add your user to the Docker group (remember to log out and back in for this to take effect).

These scritps have been tested in Linux (Ubuntu).

Next sections describe in detail how to use these scripts.

# Building the Aerostack Docker image

The script `build.sh` creates the Docker image. This script uses the the following format:

     $ ./build.sh <dockerfile_dir> <git_url> [<image_name> <gpu_support>]

If the optional parameter <gpu_support> is written with value 1, this script adds support for GPU.

The creation of a Docker image for Aerostack is normally performed by Aerostack developers when there is a stable version of Aerostack. The Aerostack image is uploaded to "Docker Hub", a library of container images provided by Docker. Aerostack uses a user account in Docker Hub called `visionaerialrobotics` in which there are images for different Aerostack versions.  

The following steps show how to build the Docker image for Aerostack 2.1:

1. Execute build.sh script to build the Docker image:

        $ ./docker_container/scripts/build.sh /docker_container/aerostack https://github.com/Vision4UAV/Aerostack aerostack_v2.1

1. Check that the image was built successfully. This will return the repository name `aerostack_v2.1` and its details in the following way (may be different from yours):

        $ sudo docker images
        REPOSITORY          TAG         IMAGE ID        CREATED         SIZE
        aerostack_v2.1      latest      8a3bf7598121    1 minute ago    6.06GB

1. Create the tag:

        $ sudo docker tag aerostack_v2.1:latest visionaerialrobotics/aerostack_v2.1

1. Log in to Docker:
 
        $ sudo docker login    
        Username: visionaerialrobotics
        Password: cvardocker

1. Push Docker image:

        $ sudo docker push visionaerialrobotics/aerostack_v2.1

You can build a Docker image for another version of Aerostack (e.g., an image from the Aerostack repository on Bitbucket). In this case, you need your Bitbucket credentials and the corresponding user rights. 

For example, for building the Docker image, named my_aerostack (considering that the Bitbucket user is myuser and the Bitbucket password is mypass) you can excute the following command:

     $ ./docker_container/scripts/build.sh ./docker_container/aerostack https://myuser:mypass@bitbucket.org/Vision4UAV/aerostack.git my_aerostack

To check that the image was built successfully, write:

     $ sudo docker images

# Using the Aerostack Docker image

In order to use the Aerostack image, the following steps can be done:

1. Download the Aerostack image
1. Create the Aerostack container (script `start.sh`)
1. Use the Aerostack container (script `launch.sh`)
1. Stops the running container (script `stop.sh`)

The following sections describe in detail these steps.

## Downloading the Aerostack Docker image

The following steps get the Aerostack image (aerostack_v2.1) from the Docker Hub:

1. Pull the image from the Docker Hub

        $ sudo docker pull visionaerialrobotics/aerostack_v2.1

1. Check that the image was downloaded successfully. This returns the container name and its details (may be different from yours):

        $ sudo docker images
        REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
        visionaerialrobotics/aerostack_v2.1     latest              8a3bf7598121        23 hours ago        6.06GB


## Creating the Aerostack Docker container

The script `start.sh` creates the Docker container setting everything up (i.e.: volumes, bound ports, and workspace). This script accepts the following parameters:

    $ ./start.sh <working_dir> <image_name> [<instance_name> <gpu_support>]

The optional parameter <instance_name> is the name of the container. If this parameter is not given, a default name is created with the name of the image and "_instance". If the optional parameter <gpu_support> is written with value 1, this script adds support for GPU.

The following steps illustrate how to do this task for the image aerostack_v2.1:

1. Create de Docker container:

        $ ./docker_container/scripts/start.sh . visionaerialrobotics/aerostack_v2.1 aerostack_v2.1_container

1. Check that the container was created successfully:

        $ sudo docker ps -l
        CONTAINER ID        IMAGE                                 COMMAND                  CREATED              STATUS              PORTS                      NAMES
        8c59d3453f1b        visionaerialrobotics/aerostack_v2.1   "/ros_entrypoint.s..."   About a minute ago   Up About a minute   0.0.0.0:11392->11311/tcp   aerostack_v2.1_container

1. Verify that the directories `workspace`, `catkin_ws`, `gazebo_ws` were created (this example shows only some of the directories):

        $ ls
        catkin_ws docker_container gazebo_ws workspace
       
## Using the Aerostack Docker container

The script `launch.sh` launches the Docker container and ensures the necessary authentication with the X11 server (for graphical user interface). This script accepts the following parameters:

    $ ./launch.sh <container_name> [<shell>]

The optional parameter <shell> is the shell type used. The default value is zsh.

The following example illustrates how to do this task for the image aerostack_v2.1:

1. Launch the Aerostack container:

        $ ./docker_container/scripts/launch.sh aerostack_v2.1_container

1. You can now use Aerostack to launch a mission:

        $ cd $AEROSTACK_STACK
        $ source ./setup.sh
        $ ./launchers/simulated_quadrotor_basic.sh

#### End of usage

When you finish using the container, exit and stop the container with the following commands:

    $ exit
    $ ./docker_container/scripts/stop.sh aerostack_v2.1_container

#### First Usage

If the container has just been created (i.e., it is the first time you use it), execute the following commands:

```sh
$ source ros_entrypoint.sh
$ source /root/post_install.sh https://github.com/Vision4UAV/Aerostack 
```

Please note that the script post_install.sh uses a URL address. If this URL address is not public and requires credentials (user name and password) this should be given in following form: https://[myuser:mypass@]github.com/Vision4UAV/Aerostack (considering that myuser and mypass are respectively the user name and password).

Compile the Aerostack code with the following commands:

```sh
$ cd $AEROSTACK_WORKSPACE
$ catkin_make
```